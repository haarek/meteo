��    L      |  e   �      p     q     w          �     �  .   �  	   �     �  &   �               %      5  :   V     �     �     �     �     �     �     �  	   �  >  �     *	     6	     ;	     I	     i	     r	     �	     �	     �	     �	     �	  
   �	     �	     �	  	   	
  ?   
  
   S
     ^
  
   o
     z
     
  2   �
     �
     �
     �
     �
     �
     �
           	  
             +     <     D     L     S     c     o     �  	   �     �     �     �     �  +   �  	   	  
          
   '     2     M  F  U     �     �     �     �     �  .   �  	   �     �  &        ,     A     P      `  :   �     �     �     �     �     �     �     �  	     >       U     a     f     t     �     �     �     �     �     �       
             $  	   4  ?   >  
   ~     �  
   �     �     �  2   �     �     �     �                    +     4  
   9     D     V     g     o     w     ~     �     �     �  	   �     �     �     �     �  +     	   4  
   >     I  
   R     ]     x         D                       8          *   '   J          	   0          -   5       4   6   E       /   $   ,   >       B          F   )   G              <          #   3           L      I   %   (            H           A   =       .      :       K          !   1          ;   9       @   2   C   7      
            +   &                           "         ?           1 hr. 12 hrs. 2 hrs. 24 hrs. 6 hrs. A forecast application with OpenWeatherMap API API key:  About Meteo At least of 3 characters are required! Based in a mockup by Carlos Suárez Change location Choose your city with popup maps Choose your units from metric, imperial or british systems Close Cloudiness : Clouds Collaborators Coord. lat:  Coord. lon:  Coordinates : Country:  Current weather, with information about temperature, pressure, wind speed and wind direction, sunrise and sunset. Know meteorologic predictions to next hours and days. Show awesome maps with all the information. Switch between some maps distributors. Now with System Tray Indicator, showing your location and forecast. Dark theme: Data Description:  Find my location automatically: Forecast Forecast App for desktop Forecast for next 18 hours Forecast for next five days Found an error General Humidity ID place:  Icon file:  Imperial System Interface Know the forecast of the next hours and days with data and maps Language:  Launch on start: Location:  Maps Meteo Meteo;Weather;Forecast;Temperature;Wind;Snow;Rain; Metric System No data Options Other features: Precipitation Preferences Pressure Quit Show Meteo Special Thanks to Start minimized: State:  Sunrise Sunset Symbolic icons: Temperature Terms of Service Today UK System Units Update conditions Update conditions every : Use System Tray Indicator: Watch awesome maps with weather information Weather:  Wind Speed Wind dir Wind speed com.gitlab.bitseater.meteo website Project-Id-Version: 
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-02-02 21:22+0100
Last-Translator: Carlos Suárez <bitseater@gmail.com>
Language-Team: 
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.2.1
Plural-Forms: nplurals=2; plural=(n != 1);
 1 hr. 12 hrs. 2 hrs. 24 hrs. 6 hrs. A forecast application with OpenWeatherMap API API key:  About Meteo At least of 3 characters are required! Based in a mockup by Carlos Suárez Change location Choose your city with popup maps Choose your units from metric, imperial or british systems Close Cloudiness : Clouds Collaborators Coord. lat:  Coord. lon:  Coordinates : Country:  Current weather, with information about temperature, pressure, wind speed and wind direction, sunrise and sunset. Know meteorologic predictions to next hours and days. Show awesome maps with all the information. Switch between some maps distributors. Now with System Tray Indicator, showing your location and forecast. Dark theme: Data Description:  Find my location automatically: Forecast Forecast App for desktop Forecast for next 18 hours Forecast for next five days Found an error General Humidity ID place:  Icon file:  Imperial System Interface Know the forecast of the next hours and days with data and maps Language:  Launch on start: Location:  Maps Meteo Meteo;Weather;Forecast;Temperature;Wind;Snow;Rain; Metric System No data Options Other features: Precipitation Preferences Pressure Quit Show Meteo Special Thanks to Start minimized: State:  Sunrise Sunset Symbolic icons: Temperature Terms of Service Today UK System Units Update conditions Update conditions every : Use System Tray Indicator: Watch awesome maps with weather information Weather:  Wind Speed Wind dir Wind speed com.gitlab.bitseater.meteo website 
<?xml version="1.0" encoding="UTF-8"?>
<!-- Copyright 2017-2019 Carlos Suárez <bitseater@gmail.com> -->
<component type="desktop-application">
    <id>com.gitlab.bitseater.meteo.desktop</id>
    <metadata_license>CC0-1.0</metadata_license>
    <project_license>GPL-3.0-or-later</project_license>
    <update_contact>bitseater@gmail</update_contact>
    <name>Meteo</name>
    <translation type="gettext">com.gitlab.bitseater.meteo</translation>
    <summary>Know the forecast of the next hours and days with data and maps</summary>
    <description>
        <p>Current weather, with information about temperature, pressure,
        wind speed and wind direction, sunrise and sunset. Know meteorologic
        predictions to next hours and days. Show awesome maps with all the
        information. Switch between some maps distributors. Now with System
        Tray Indicator, showing your location and forecast.</p>
        <p>Other features: </p>
        <ul>
            <li>Forecast for next 18 hours</li>
            <li>Forecast for next five days</li>
            <li>Choose your units from metric, imperial or british systems</li>
            <li>Choose your city with popup maps</li>
            <li>Watch awesome maps with weather information</li>
        </ul>
    </description>
    <provides>
        <binary>com.gitlab.bitseater.meteo</binary>
    </provides>
    <screenshots>
        <screenshot type="default">
            <image type="source" width="898" height="598">https://gitlab.com/bitseater/meteo/raw/master/data/screens/screenshot_1.png</image>
        </screenshot>
        <screenshot type="default">
            <image type="source" width="898" height="598">https://gitlab.com/bitseater/meteo/raw/master/data/screens/screenshot_2.png</image>
        </screenshot>
        <screenshot type="default">
            <image type="source" width="898" height="598">https://gitlab.com/bitseater/meteo/raw/master/data/screens/screenshot_3.png</image>
        </screenshot>
        <screenshot type="default">
            <image type="source" width="898" height="598">https://gitlab.com/bitseater/meteo/raw/master/data/screens/screenshot_4.png</image>
        </screenshot>
    </screenshots>
    <developer_name>Carlos Suárez</developer_name>
    <url type="homepage">https://gitlab.com/bitseater/meteo</url>
    <url type="bugtracker">https://gitlab.com/bitseater/meteo/issues</url>
    <releases>
        <release version="0.9.5" date="2019-02-02">
        </release>
    </releases>
    <content_rating type="oars-1.0">
        <content_attribute id="violence-cartoon">none</content_attribute>
        <content_attribute id="violence-fantasy">none</content_attribute>
        <content_attribute id="violence-realistic">none</content_attribute>
        <content_attribute id="violence-bloodshed">none</content_attribute>
        <content_attribute id="violence-sexual">none</content_attribute>
        <content_attribute id="violence-desecration">none</content_attribute>
        <content_attribute id="violence-slavery">none</content_attribute>
        <content_attribute id="violence-worship">none</content_attribute>
        <content_attribute id="drugs-alcohol">none</content_attribute>
        <content_attribute id="drugs-narcotics">none</content_attribute>
        <content_attribute id="drugs-tobacco">none</content_attribute>
        <content_attribute id="sex-nudity">none</content_attribute>
        <content_attribute id="sex-themes">none</content_attribute>
        <content_attribute id="sex-homosexuality">none</content_attribute>
        <content_attribute id="sex-prostitution">none</content_attribute>
        <content_attribute id="sex-adultery">none</content_attribute>
        <content_attribute id="sex-appearance">none</content_attribute>
        <content_attribute id="language-profanity">none</content_attribute>
        <content_attribute id="language-humor">none</content_attribute>
        <content_attribute id="language-discrimination">none</content_attribute>
        <content_attribute id="social-chat">none</content_attribute>
        <content_attribute id="social-info">none</content_attribute>
        <content_attribute id="social-audio">none</content_attribute>
        <content_attribute id="social-location">none</content_attribute>
        <content_attribute id="social-contacts">none</content_attribute>
        <content_attribute id="money-purchasing">none</content_attribute>
        <content_attribute id="money-gambling">none</content_attribute>
    </content_rating>
</component>
